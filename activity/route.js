/*
9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.
11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.
*/

let http = require("http");

let port = 3000;

let server = http.createServer(function (request, response){
	if (request.url==="/login"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the login page!");
	}
	// else if (request.url==="/homepage"){
	// 	response.writeHead(200, {"Content-Type": "text/plain"});
	// 	response.end("Hello");
	// }
	else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("The page you are looking for does not exist.");	
	}
});
server.listen(port);
console.log(`Server now running at port: ${port}`);
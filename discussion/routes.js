const http = require("http");

// storing the 400 in a variable colled port

const port = 4000;

// storing the createServer method inside the server variable
const server = http.createServer(function (request, response){
	if (request.url==="/greeting"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello Again");
	}
	else if (request.url==="/homepage"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello");
	}
	else {
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");	
	}
});

server.listen(port);
console.log(`Server now running at port: ${port}`);

// "request" is an object that is sent via the client (browser) 
// "url" is the property of the request that refers to the endpoint of the link
// The condition (if) above means that we have to access the localhost:4000 with "/greeting" in its endpoint

// mini-activity
/*
create another response "Hello" with 200 as the status code and plain text as the contenty type when we try to access "/homepage" endpoint
*/
// Solution" so idudugtong mo lang siya sa taas as an else if statement condition
/*
else if (request.url==="/homepage"){
	response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Hello")
}
*/

// panu naman magset ng default response pagdating sa mga webpage na di natin sineset? set the condition sa else statement
/*
we use "require" directive to load Node.js modules
- si require and access natin sa mga modules 
- module is a software component or a part of a program that contained one or more routines
- hypertext transfer protocol
"http" module lets Node.js transfer data using Hyper Text Transfer Protocol
- set of individual files that contain code to create a 'component' that helps establish data transfer between applications
-  a protocol that allows the fetching of resources such as HTML documents

Clients(browser) and server (Node.js/express.js applications) 
message that came from the clients - request
message that came from the server - response

*/


let http = require("http");

/*
http - we are now tryng to use the http module for us to create our server-side application 

createServer() - found inside http module; a method that accepts a function as its argument for a creation of a server

(request, response) - arguments that are passed to the createServer method; this would allow us to receive requests (1st parameter) and send responses (2nd parameter)

.listen() - allows our application to be run in our local devices through a specified port

port - virtual point where network connections start and end (sa local host or devices lang sila nagrarun)
*/
http.createServer(function (request, response){
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Hello World");

}).listen(4000);
// this code means that the server will be assigned to port 4000 via .listen(4000) method where the server will listen to any requests that are sent to it eventually communicating with our server

/*
use node index.js to run the server
press ctrl+c to terminate the gitbash process

*/

// used to confirm if the server is running on a port
console.log("Server running at port 4000");
// gitbash terminal is our console

// laman ng function:
// we use writeHead() to set a status code for the response. a 200 means OK or successful status
/*
some ommon HTTP status codes:
1. 200 - successful request
2. 301 - moved permanently
3. 401 - unauthorized request
4. 403 - forbidden 
5. 500 - internal server error
*/
// set COntent-Type; using "text/plain" means that we are sending plain text as a response 

// we use responnse.end to denote the last stage of the communication which is the sending of the response from the server

/*
nodemon
- installing this package will allow the server to automatically restrat when files have been changed for update i.e. saving the files
- npm install -g nodemon:
	- "npm install" means we are going to access the npm and install one of the packages that is in its library

	- "-g" means we are going to install the package globally in our device. This means that we are on the other directories/repositories in our device, we could sill use nodemon
	- "nodemon" - 
*/
/*
. Import the http module using the required directive.
6. Create a variable port and assign it with the value of 3000.
7. Create a server using the createServer method that will listen in to the port provided above.
8. Console log in the terminal a message when the server is successfully running.
9. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
10. Access the login route to test if it’s working as intended.
11. Create a condition for any other routes that will return an error message.
12. Access any other route to test if it’s working as intended.


*/

let http = require("http");

http.createServer(function (request, response){
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.end("Activity 31");

}).listen(3000);

console.log("Server running at port 3000");
